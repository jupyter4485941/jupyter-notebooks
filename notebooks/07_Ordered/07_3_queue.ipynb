{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "0",
   "metadata": {},
   "source": [
    "## 7.3 Queues"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1",
   "metadata": {},
   "source": [
    "A stack is a sequence where items are added and removed from the same end.\n",
    "A queue is a sequence where items are added to one end and removed from the other."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2",
   "metadata": {},
   "source": [
    "### 7.3.1 Single- and double-ended queues"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3",
   "metadata": {},
   "source": [
    "People queue when boarding planes; cars queue at drive-ins.\n",
    "People and cars join the queue at the end and leave it at the front.\n",
    "Like stacks, queues are sequences of items ordered by arrival order, but\n",
    "contrary to stacks, the item arriving first is also the first to be processed:\n",
    "the 'oldest' item is at the bottom of the stack but at the front of the queue.\n",
    "A **queue** is a **first in, first out (FIFO)** sequence.\n",
    "The operations are very similar to a stack's push, pop, and peek.\n",
    "\n",
    "Operation | Effect | Algorithm in English\n",
    ":-|:-|:-\n",
    "new  | create an empty queue *q* | let *q* be an empty queue\n",
    "length | the number of items in *q*  | │*q*│\n",
    "enqueue | add an item to the back of *q*  | add *item* to *q*\n",
    "dequeue | remove the front item, if *q* isn't empty |  dequeue *q*\n",
    "front | access the front item of, if *q* isn't empty | front of *q*\n",
    "\n",
    "Alternatively, the dequeue operation could remove and return the front item,\n",
    "but this wouldn't make the front operation redundant. Why?\n",
    "\n",
    "___\n",
    "\n",
    "With stacks, we can inspect the top item without 'disturbing' the stack,\n",
    "by popping it and pushing it immediately back.\n",
    "We can't inspect the front item of a queue with a dequeue followed by an\n",
    "enqueue operation: the front item would end up at the back of the queue.\n",
    "\n",
    "A **deque** (pronounced 'deck'), short for **double-ended queue**, is a sequence\n",
    "where items can be removed from and added to the front and the end.\n",
    "Besides 'new' and 'length', the operations are:\n",
    "\n",
    "Operation | Effect | Algorithm in English\n",
    ":-|:-|:-\n",
    "prepend | add an item at the front of deque *d*  | prepend *item* to *d*\n",
    "append | add an item to the back of *d* |  append *item* to *d*\n",
    "remove front | remove the front item, if *d* isn't empty | remove front of *d*\n",
    "remove back | remove the back item, if *d* isn't empty | remove back of *d*\n",
    "front | access the front item, if *d* isn't empty | front of *d*\n",
    "back | access the back item, if *d* isn't empty | back of *d*"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-info\">\n",
    "<strong>Info:</strong> There are no standard names for the deque operations.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5",
   "metadata": {},
   "source": [
    "#### Exercise 7.3.1"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6",
   "metadata": {},
   "source": [
    "How would you represent a to-do list of tasks:\n",
    "with a general sequence, a stack, a queue or a deque?\n",
    "\n",
    "[Hint](../31_Hints/Hints_07_3_01.ipynb)\n",
    "[Answer](../32_Answers/Answers_07_3_01.ipynb)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7",
   "metadata": {},
   "source": [
    "#### Exercise 7.3.2 (optional)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8",
   "metadata": {},
   "source": [
    "Define the queue and deque operations in the same way as the\n",
    "[stack operations](../07_Ordered/07_1_stack.ipynb#7.1-Stacks), using sequence notation.\n",
    "The operations are similar, so you may wish to define only one or two of them."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9",
   "metadata": {},
   "source": [
    "### 7.3.2 Queues with Python lists"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "10",
   "metadata": {},
   "source": [
    "Python's versatile lists can not only simulate stacks but also queues."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "11",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Next person served: Alice\n",
      "Next person served: Bob\n",
      "Next person served: Clara\n",
      "People still waiting? False\n"
     ]
    }
   ],
   "source": [
    "queue = []\n",
    "queue.append('Alice')   # Alice arrives first\n",
    "queue.append('Bob')\n",
    "print('Next person served:', queue.pop(0))\n",
    "queue.append('Clara')\n",
    "print('Next person served:', queue.pop(0))\n",
    "print('Next person served:', queue.pop(0))\n",
    "print('People still waiting?', len(queue) > 0)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "12",
   "metadata": {},
   "source": [
    "This approach puts the front of the queue at index&nbsp;0 and therefore uses\n",
    "`append` as the enqueue operation and  `pop(0)` as the dequeue operation.\n",
    "The downside is that the latter takes time linear in the length of the list,\n",
    "because each remaining item is shifted one position down.\n",
    "\n",
    "We could instead have the front of the queue at the last index.\n",
    "That would make dequeuing take constant time,\n",
    "but enqueuing would take linear time.\n",
    "\n",
    "To sum up, using lists and their methods makes one queue operation take linear\n",
    "time, which may or may not be acceptable for the application at hand."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "13",
   "metadata": {},
   "source": [
    "#### Exercise 7.3.3"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "14",
   "metadata": {},
   "source": [
    "Python lists can represent deques too. Complete the following table,\n",
    "indicating for each deque operation the corresponding list operation,\n",
    "and its complexity. I've done the first two for you.\n",
    "\n",
    "Deque operation | List operation | Complexity\n",
    "-|-|-\n",
    "new | `a_deque = []` | Θ(1)\n",
    "length  | `len(a_deque)`  | Θ(1)\n",
    "append | |\n",
    "prepend |  |\n",
    "remove front |  |\n",
    "remove back |  |\n",
    "front   |   |\n",
    "back   |   |\n",
    "\n",
    "[Hint](../31_Hints/Hints_07_3_03.ipynb)\n",
    "[Answer](../32_Answers/Answers_07_3_03.ipynb)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "15",
   "metadata": {},
   "source": [
    "### 7.3.3 Deques in Python"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "16",
   "metadata": {},
   "source": [
    "Python includes an implementation of the deque ADT.\n",
    "The `collections` module includes class `deque` with methods `append` and `pop`\n",
    "to add and remove items at the back, and methods\n",
    "`appendleft` and `popleft` to add and remove items at the front.\n",
    "The advantage of using the `deque` data type is\n",
    "that all four methods take constant time.\n",
    "\n",
    "If we only use `append` and `pop` then the deque behaves like a stack,\n",
    "although there's no point in using `deque` for that purpose."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "17",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Next person served: Bob\n",
      "Next person served: Clara\n",
      "Next person served: Alice\n"
     ]
    }
   ],
   "source": [
    "from collections import deque\n",
    "\n",
    "stack = deque()\n",
    "stack.append('Alice')\n",
    "stack.append('Bob')\n",
    "print('Next person served:', stack.pop())\n",
    "stack.append('Clara')\n",
    "print('Next person served:', stack.pop())\n",
    "print('Next person served:', stack.pop())"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "18",
   "metadata": {},
   "source": [
    "As expected with a LIFO policy,\n",
    "the last person to arrive is the first to be served.\n",
    "Alice isn't too happy about it.\n",
    "Note that `deque`'s `pop` method requires no argument.\n",
    "\n",
    "If we only use `append` and `popleft`, the deque behaves like a queue:\n",
    "first come, first served."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "19",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Next person served: Alice\n",
      "Next person served: Bob\n",
      "Next person served: Clara\n"
     ]
    }
   ],
   "source": [
    "queue = deque()\n",
    "queue.append('Alice')\n",
    "queue.append('Bob')\n",
    "print('Next person served:', queue.popleft())\n",
    "queue.append('Clara')\n",
    "print('Next person served:', queue.popleft())\n",
    "print('Next person served:', queue.popleft())"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "20",
   "metadata": {},
   "source": [
    "If we use all four methods, we can simulate people getting tired of waiting and\n",
    "leaving the back of the queue or people jumping the queue and joining at the\n",
    "front, which is almost a criminal offence in the UK."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "21",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-info\">\n",
    "<strong>Info:</strong> In Java, deque operations are defined by interface <code>Deque</code>.\n",
    "It's usually implemented by class <code>ArrayDeque</code> or <code>LinkedList</code>.\n",
    "Both the interface and the classes are in package <code>java.util</code>.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "22",
   "metadata": {},
   "source": [
    "### 7.3.4 Using queues"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "23",
   "metadata": {},
   "source": [
    "Consider *n* children in a circle, numbered clockwise from 1 to *n*.\n",
    "Alice is in the centre. She points at the first child and starts reciting:\n",
    "\n",
    "> Eeny, meeny, miny, moe,\\\n",
    "> Catch a tiger by the toe.\\\n",
    "> If he hollers, let him go,\\\n",
    "> Eeny, meeny, miny, moe.\n",
    "\n",
    "For each syllable, Alice points at a child, going clockwise.\n",
    "For example, with *n* = 3, she would point successively at\n",
    "children 1 (ee) 2 (ny) 3 (mee) 1 (ny) 2 (mi) 3 (ny) 1 (moe) for the first line.\n",
    "The child pointed to on the last syllable, the second 'moe', leaves the circle.\n",
    "The reciting and counting starts again on the next child.\n",
    "After going *n* − 1 times through the rhyme, one child is left in the circle.\n",
    "We want to know which child is that."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "24",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-info\">\n",
    "<strong>Info:</strong> This is a version of the\n",
    "<a href=\"https://en.wikipedia.org/wiki/Josephus_problem\">Josephus problem</a>.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "25",
   "metadata": {},
   "source": [
    "#### Exercise 7.3.4"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "26",
   "metadata": {},
   "source": [
    "In an [earlier exercise](../04_Iteration/04_5_tuples.ipynb#Exercise-4.5.2)\n",
    "we saw that a rectangular board doesn't have to be represented by a table.\n",
    "For this problem, we can represent a circle of children as a queue. How?"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "27",
   "metadata": {},
   "source": [
    "_Write your answer here._"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "28",
   "metadata": {},
   "source": [
    "[Hint](../31_Hints/Hints_07_3_04.ipynb)\n",
    "[Answer](../32_Answers/Answers_07_3_04.ipynb)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "29",
   "metadata": {},
   "source": [
    "#### Exercise 7.3.5"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "30",
   "metadata": {},
   "source": [
    "Given the number of children *n*, we want to know the number of\n",
    "the last remaining child. Outline an algorithm to compute that number."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "31",
   "metadata": {},
   "source": [
    "_Write your answer here._"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "32",
   "metadata": {},
   "source": [
    "[Hint](../31_Hints/Hints_07_3_05.ipynb)\n",
    "[Answer](../32_Answers/Answers_07_3_05.ipynb)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "33",
   "metadata": {},
   "source": [
    "#### Exercise 7.3.6"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "34",
   "metadata": {},
   "source": [
    "Implement the algorithm you outlined by completing the following function.\n",
    "Use Python's `deque` data type instead of lists."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "35",
   "metadata": {},
   "outputs": [],
   "source": [
    "from collections import deque\n",
    "%run -i ../m269_util\n",
    "\n",
    "def counting_rhyme(n: int) -> int:\n",
    "    \"\"\"Return which child from 1 to n remains last in the circle.\n",
    "\n",
    "    Preconditions: n > 0\n",
    "    \"\"\"\n",
    "    pass\n",
    "\n",
    "counting_rhyme_tests = [\n",
    "    # case,         n,  last child\n",
    "    ['1 child',     1,          1],\n",
    "    ['2 children',  2,          1],\n",
    "    ['3 children',  3,          2]\n",
    "]\n",
    "\n",
    "test(counting_rhyme, counting_rhyme_tests)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "36",
   "metadata": {},
   "source": [
    "[Hint](../31_Hints/Hints_07_3_06.ipynb)\n",
    "[Answer](../32_Answers/Answers_07_3_06.ipynb)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "37",
   "metadata": {},
   "source": [
    "#### Exercise 7.3.7"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "38",
   "metadata": {},
   "source": [
    "What is the complexity of the algorithm as implemented in the solution to the previous exercise?"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "39",
   "metadata": {},
   "source": [
    "_Write your answer here._"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "40",
   "metadata": {},
   "source": [
    "[Hint](../31_Hints/Hints_07_3_07.ipynb)\n",
    "[Answer](../32_Answers/Answers_07_3_07.ipynb)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "41",
   "metadata": {},
   "source": [
    "#### Optional exercises"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "42",
   "metadata": {},
   "source": [
    "Further problems involving queues are listed in the\n",
    "[Kattis Guide](https://mwermelinger.github.io/kattis-guide/ordered.html#queues)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "43",
   "metadata": {},
   "source": [
    "⟵ [Previous section](07_2_stack_usage.ipynb) | [Up](07-introduction.ipynb) | [Next section](07_4_queue_implementation.ipynb) ⟶"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.11"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
