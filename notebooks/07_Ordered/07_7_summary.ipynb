{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "0",
   "metadata": {},
   "source": [
    "## 7.7 Summary"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1",
   "metadata": {},
   "source": [
    "For some problems, it's useful to have sequences that restrict which\n",
    "items can be accessed and removed from the sequence.\n",
    "\n",
    "In all of the following, operations to access and remove items are only defined\n",
    "for non-empty sequences."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2",
   "metadata": {},
   "source": [
    "### 7.7.1 Stacks"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3",
   "metadata": {},
   "source": [
    "A **stack** is a **last-in, first-out** (**LIFO**) sequence:\n",
    "only the most recently added item can be accessed or removed.\n",
    "The stack ADT supports these operations:\n",
    "\n",
    "Operation | English | Python\n",
    "-|-|-\n",
    "new stack | let *s* be a new stack | `s = []`\n",
    "size | │*s*│ | `len(s)`\n",
    "push (add item) | push *item* on *s* | `s.append(item)`\n",
    "peek (last item added) | top of *s* | `s[-1]`\n",
    "pop (remove last item) | pop *s* | `s.pop(-1)`\n",
    "\n",
    "When translating an algorithm from English to Python,\n",
    "we can combine the peek and pop operations when useful and\n",
    "write `item = s.pop(-1)`.\n",
    "\n",
    "The top item of a stack is stored in the last element of an array or\n",
    "in the first node of a linked list, in order to implement all operations\n",
    "in constant time.\n",
    "\n",
    "Stacks can be used to evaluate postfix expressions,\n",
    "where operators are written after its operands.\n",
    "Contrary to infix expressions, where operators are written between operands,\n",
    "postfix expressions don't need parentheses to indicate the order of operations."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4",
   "metadata": {},
   "source": [
    "### 7.7.2 Queues"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5",
   "metadata": {},
   "source": [
    "A **queue** is a **first-in, first-out** (**FIFO**) sequence:\n",
    "items are removed in the order they arrived.\n",
    "\n",
    "A **deque** (**double-ended queue**) is a sequence where items can be accessed,\n",
    "added and removed from either end.\n",
    "Deques can simulate stacks and queues.\n",
    "\n",
    "The queue ADT provides the following operations,\n",
    "implemented by Python's `deque` class in module `collections`.\n",
    "\n",
    "Operation | English | Python\n",
    ":-|:-|:-\n",
    "new  | let *q* be an empty queue | `q = deque()`\n",
    "length | │*q*│ | `len(q)`\n",
    "enqueue | add *item* to *q* | `q.append(item)`\n",
    "dequeue |  dequeue *q* | `q.popleft()`\n",
    "front | front of *q* | `q[0]`\n",
    "\n",
    "The deque ADT provides the following operations, besides new and length:\n",
    "\n",
    "Operation | English | Python\n",
    ":-|:-|:-\n",
    "prepend | prepend *item* to *d* | `d.appendleft(item)`\n",
    "append | append *item* to *d* | `d.append(item)`\n",
    "remove front | remove front of *d* | `d.popleft()`\n",
    "remove back | remove back of *d* | `d.pop()`\n",
    "front | front of *d* | `d[0]`\n",
    "back | back of *d* | `d[-1]`\n",
    "\n",
    "A queue (or deque) can be implemented as\n",
    "a singly (respectively, doubly) linked list of nodes.\n",
    "In a **singly linked list**, each node points to the next one;\n",
    "in a **doubly linked list**, each node also points to the previous one.\n",
    "\n",
    "All operations on queues and deques take constant time."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6",
   "metadata": {},
   "source": [
    "### 7.7.3 Priority queues"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7",
   "metadata": {},
   "source": [
    "A **priority queue** is a sequence of items, each with an associated priority.\n",
    "Items are processed from highest to lowest priority value in a max-priority\n",
    "queue, and from lowest to highest priority value in a min-priority queue.\n",
    "Unless told otherwise,\n",
    "we can't assume in which order items with the same priority are processed.\n",
    "\n",
    "Priorities can be of any data type with comparable values.\n",
    "For integer priorities, a max-priority queue can be used as\n",
    "a min-priority queue by negating the priorities when adding items.\n",
    "\n",
    "A max-priority queue ADT provides the following operations, implemented by\n",
    "class `ArrayPriorityQueue` in file `m269_priority.py`.\n",
    "\n",
    "Operation | English | Python\n",
    ":-|:-|:-\n",
    "new | let *pq* be a new priority queue | `pq = ArrayPriorityQueue()`\n",
    "length |  │*pq*│ | `pq.length()`\n",
    "insert  | add (*priority*, *item*) to *pq* | `pq.insert(item, priority)`\n",
    "find max | max(*pq*) | `pq.find_max()`\n",
    "remove max | remove max(*pq*) | `pq.remove_max()`\n",
    "\n",
    "All operations take constant time except that inserting an item is\n",
    "linear in the length of the priority queue."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8",
   "metadata": {},
   "source": [
    "⟵ [Previous section](07_6_priority_queue.ipynb) | [Up](07-introduction.ipynb) | [Next section](../08_Unordered/08-introduction.ipynb) ⟶"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.11"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
